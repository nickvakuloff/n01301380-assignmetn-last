﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class Races : System.Web.UI.Page
    {
        private string basequery { get; set; } = "SELECT race_id, race_name, race_leader, race_capital, race_description FROM Races";
        protected void Page_Load(object sender, EventArgs e)
        {
            races_query.InnerHtml = basequery;
            races_select.SelectCommand = basequery;
            //characters_list.DataSource = characters_select;
            races_list.DataSource = Races_Page_Bind(races_select);
            races_list.DataBind();
        }
        protected DataView Races_Page_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                //row["race_name"] =
                //    "<a href=\"Class.aspx?classid="
                //    + row["race_id"]
                //    + "\">"
                //    + row["race_name"]
                //    + "</a>";
            }
            mytbl.Columns.Remove("race_id");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}