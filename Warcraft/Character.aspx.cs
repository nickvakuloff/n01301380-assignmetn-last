﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class Character : System.Web.UI.Page
    {
        private string character_basequery = "SELECT character_id, character_name AS 'Name', faction_name AS 'Faction', race_name AS 'Race', character_bio AS 'Bio', character_author AS 'Author' FROM CHARACTERS JOIN races ON races.race_id = characters.character_race JOIN factions ON factions.faction_id = characters.character_faction";
        public string character_id
        {
            get { return Request.QueryString["character_id"]; }
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //make the "add this student to class" div invisible, if there is a
            //student, make it visible. 
            if (character_id == "" || character_id == null)
            {
                del_debug.InnerHtml = "test123555";
                //character_name.InnerHtml = "No character found.";
                return;
            }
            else
            {
                //We can use the default data bind here
                //nothing special we need to do to manipulate the rendering
                character_basequery += " WHERE character_id = " + character_id;
                //debug.InnerHtml = student_basequery;
                character_select.SelectCommand = character_basequery;
                character_query.InnerHtml = character_basequery;
                //debug.InnerHtml = student_basequery;


                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView characterview = (DataView)character_select.Select(DataSourceSelectArguments.Empty);

                //If there is no student in the studentview, an invalid id is passed
                if (characterview.ToTable().Rows.Count < 1)
                {
                    character_name.InnerHtml = "No character found 1231231.";
                    return;
                }

                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView scharacterrowview = characterview[0]; //gets the first result which is always the student
                string charactername = characterview[0]["Name"].ToString();
                character_name.InnerHtml = charactername;

                character_list.DataSource = character_select;
                character_list.DataBind();
            }
        }
        protected void DelClass(object sender, EventArgs e)
        {
            //Don't want to use a base string for delete 
            //because "DELETE FROM CLASSES"
            //should never be executed, it's very dangerous to delete!
            string delquery = "DELETE FROM characters WHERE character_id=" + character_id;

            del_debug.InnerHtml = delquery;
            del_character.DeleteCommand = delquery;
            del_character.Delete();
        }
    }
}