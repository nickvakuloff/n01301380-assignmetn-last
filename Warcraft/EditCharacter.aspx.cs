﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class EditCharacter : System.Web.UI.Page
    {
        public int character_id
        {
            get { return Convert.ToInt32(Request.QueryString["character_id"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView characterrow = getCharacterInfo(character_id);
            if (characterrow == null)
            {
                character_fullname.InnerHtml = "No Character Found.";
                return;
            }
            character_name.Text = characterrow["character_name"].ToString();
            //debug.InnerHtml = studentrow["classcode"].ToString().Substring(0, 4).ToUpper();
            character_faction.SelectedValue = characterrow["character_faction"].ToString();
            race_name.Text = characterrow["character_race"].ToString();
            character_bio.Text = characterrow["character_bio"].ToString();
            character_author.Text = characterrow["character_author"].ToString();

        }

        protected void Edit_Character(object sender, EventArgs e)
        {
            //change this
            string name = character_name.Text;
            string faction = character_faction.SelectedValue;
            string race = race_name.Text;
            string bio = character_bio.Text;
            string author = character_author.Text;
            /*
            string startdate = student_section.SelectedValue.ToString();
            string enddate = 
            */


            string editquery = "Update characters set character_name='" + name + "'," +
                " character_faction=" + faction + ", character_race=" + race + ", character_bio ='" + bio + " character_author='" + author + "'"+
                " where character_id=" + character_id;
            debug.InnerHtml = editquery;

            //edit_character.UpdateCommand = editquery;
            //edit_character.Update();


        }

        protected DataRowView getCharacterInfo(int id)
        {
            string query = "select * from characters where character_id=" + character_id.ToString();
            character_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the students name outside of
            //the datagrid.
            DataView characterview = (DataView)character_select.Select(DataSourceSelectArguments.Empty);

            //If there is no student in the studentview, an invalid id is passed
            if (characterview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView characterrowview = characterview[0]; //gets the first result which is always the student
            //string studentinfo = studentview[0][colname].ToString();
            return characterrowview;

            //}
        }
    }
}