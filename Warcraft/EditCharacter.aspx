﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCharacter.aspx.cs" Inherits="Warcraft.EditCharacter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="character_fullname">Edit Character</h3>
    <asp:SqlDataSource runat="server" id="edit_character"
        ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="character_select"
        ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">

    </asp:SqlDataSource>
    <div class="inputrow">
        <label>Character Name:</label>
        <asp:TextBox ID="character_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="character_name"
            ErrorMessage="Enter a character name">
        </asp:RequiredFieldValidator>
    </div>
     <div class="inputrow">
    <label>Faction</label>
    
    <asp:DropDownList ID="character_faction" runat="server">
        <asp:ListItem value="1" Text="Alliance"></asp:ListItem>
        <asp:ListItem value="2" Text="Horde"></asp:ListItem>
    </asp:DropDownList>
    </div>
    <div class="inputrow">
        <label>Character Race:</label>
        <asp:TextBox ID="race_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="race_name"
            ErrorMessage="Enter a character race">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Character Bio:</label>
        <asp:TextBox ID="character_bio" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="character_bio"
            ErrorMessage="Enter a character bio">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Author:</label>
        <asp:TextBox ID="character_author" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="race_name"
            ErrorMessage="Enter a name of the author">
        </asp:RequiredFieldValidator>
    </div>
    <ASP:Button Text="Edit Character" runat="server" OnClick="Edit_Character"/>

    <div runat="server" id="debug" class="querybox"></div>
</asp:Content>
