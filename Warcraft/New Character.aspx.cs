﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class New_Character : System.Web.UI.Page
    {
        private string addquery = "INSERT into characters " +
           "(character_name, character_faction, character_race, character_bio, character_author)" +
           " VALUES ";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void AddCharacter(object sender, EventArgs e)
        {
            //string char_id = character_id.Text;
            string char_name = character_name.Text;
            string char_faction = character_faction.SelectedValue;
            string char_race = character_race.Text;
            string char_bio = character_bio.Text;
            string char_author = character_author.Text;
            //This is a little silly but we're getting the current date
            //from a date time > putting it into a string and
            //converting it back into a datetime in our insert statement.
            string now = DateTime.Today.ToString("dd/MM/yyyy");

            addquery += "( '" + char_name + "', '" +
                char_faction + "','" + char_race +  "','" + char_bio + "','" + char_author + "')";

            insert_chracter.InsertCommand = addquery;
            insert_chracter.Insert();
            debug.InnerHtml = addquery;

        }
    }
}