﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Factions.aspx.cs" Inherits="Warcraft.Factions" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="factions_select"
       ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <div id="factions_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="factions_list" runat="server" >
    </asp:DataGrid>
</asp:Content>
