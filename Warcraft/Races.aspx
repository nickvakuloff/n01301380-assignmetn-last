﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Races.aspx.cs" Inherits="Warcraft.Races" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="races_select"
       ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <div id="races_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="races_list" runat="server" >
    </asp:DataGrid>
</asp:Content>
