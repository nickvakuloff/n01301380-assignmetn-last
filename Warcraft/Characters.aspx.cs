﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class Characters : System.Web.UI.Page
    {
        private string basequery { get; set; } = "SELECT character_id, character_name AS 'Name', faction_name AS 'Faction', race_name AS 'Race', character_bio AS 'Bio' FROM CHARACTERS JOIN races ON races.race_id = characters.character_race JOIN factions ON factions.faction_id = characters.character_faction";

        protected void Page_Load(object sender, EventArgs e)
        {
            characters_query.InnerHtml = basequery;
            characters_select.SelectCommand = basequery;
            characters_list.DataSource = characters_select;
            characters_list.DataSource = Characters_Page_Bind(characters_select);
            characters_list.DataBind();
        }
        protected DataView Characters_Page_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Name"] =
                    "<a href=\"Character.aspx?character_id="
                    + row["character_id"]
                    + "\">"
                    + row["Name"]
                    + "</a>";
            }
            mytbl.Columns.Remove("character_id");
            myview = mytbl.DefaultView;

            return myview;
        }
        protected void Search_Faction(object sender, EventArgs e)
        {
            /*
              This function takes the inputs from the search
              and changes the sqldatasource select command
              to filter down students.
            */
            string newsql = basequery + " WHERE (1=1) ";
            string key = faction_key.Text;
            List<string> sec = new List<string>();
            foreach (ListItem section in faction_sec.Items)
            {
                if (section.Selected)
                {
                    sec.Add(section.Value);
                }
            }

            if (key != "")
            {
                newsql +=
                    " AND character_name LIKE '%" + key + "%'" +
                    " OR race_name LIKE '%" + key + "%' ";
            }
            if (sec.Count > 0)
            {
                //This block only executes when there is at least one section
                //selected, meaning the logic won't break if none are.
                newsql += " AND ( ";
                //Adding a condition where students can be in one,
                // 
                foreach (string factionkey in sec)
                {
                    newsql +=
                        "faction_name LIKE '%" + factionkey + "%' OR ";
                }
                newsql += " 1=0)";
                //1=0 means FALSE.
                //( ... ) OR FALSE will only be true  
                //when ( ... ) is true.
            }

            characters_select.SelectCommand = newsql;
            characters_query.InnerHtml = newsql;

            //Intercept the "auto bind" and create our own binding
            characters_list.DataSource = Characters_Page_Bind(characters_select);

            characters_list.DataBind();

        }
    }
}