﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Characters.aspx.cs" Inherits="Warcraft.Characters" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div id="inputrow">
        <a href="New Character.aspx">New Character</a>
    </div>
    <div id="faction">
        <asp:CheckBoxList runat="server" ID="faction_sec">
            <asp:ListItem Value="Alliance" Text="Alliance"></asp:ListItem>
            <asp:ListItem Value="Horde" Text="Horde"></asp:ListItem>
        </asp:CheckBoxList>
    </div>
    <asp:TextBox runat="server" ID="faction_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Faction"/>
    <asp:SqlDataSource runat="server" id="characters_select"
       ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <div id="characters_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="characters_list" runat="server" >
    </asp:DataGrid>
</asp:Content>
