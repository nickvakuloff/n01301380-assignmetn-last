﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Character.aspx.cs" Inherits="Warcraft.Character" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 id="character_name" runat="server"></h3>

    <%-- data source object which shows base info about the student --%>
   <asp:SqlDataSource runat="server"
        id="character_select"
        ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <%-- container for representing base info  --%>
    <div id="character_query" runat="server" class="querybox">

    </div>
    <asp:DataGrid ID="character_list" runat="server">

    </asp:DataGrid>
    <asp:SqlDataSource 
        runat="server"
        id="del_character"
        ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <div id="author" runat="server">

    </div>
    <asp:Button runat="server" id="del_character_btn"
        OnClick="DelClass"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="del_debug" class="querybox" runat="server"></div>
    <a href="EditCharacter.aspx?character_id=<%Response.Write(this.character_id);%>">Edit</a>
</asp:Content>
