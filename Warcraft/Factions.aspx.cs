﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class Factions : System.Web.UI.Page
    {
        private string basequery { get; set; } = "SELECT faction_id, faction_name, faction_description FROM factions";
        protected void Page_Load(object sender, EventArgs e)
        {
            factions_query.InnerHtml = basequery;
            factions_select.SelectCommand = basequery;
            //characters_list.DataSource = characters_select;
            factions_list.DataSource = Factions_Page_Bind(factions_select);
            factions_list.DataBind();
        }
        protected DataView Factions_Page_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                //row["faction_name"] =
                //    "<a href=\"Class.aspx?classid="
                //    + row["faction_id"]
                //    + "\">"
                //    + row["faction_name"]
                //    + "</a>";
            }
            mytbl.Columns.Remove("faction_id");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}