﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="New Character.aspx.cs" Inherits="Warcraft.New_Character" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="insert_chracter"
        ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>

    <h3>New Character</h3>
    <div class="inputrow">
        <label>Name:</label>
        <asp:textbox id="character_name" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Faction:</label>
        <asp:radiobuttonlist id="character_faction" runat="server">
            <asp:listitem runat="server" text="Alliance" value="1"></asp:listitem>
            <asp:listitem runat="server" text="Horde" value="2"></asp:listitem>
        </asp:radiobuttonlist>
    </div>

    <div class="inputrow">
        <label>Race:</label>
        <asp:textbox id="character_race" runat="server">
        </asp:textbox>
    </div>

     <div class="inputrow">
        <label>Bio:</label>
        <asp:textbox id="character_bio" runat="server">
        </asp:textbox>
    </div>
    <div class="inputrow">
        <label>Author:</label>
        <asp:textbox id="character_author" runat="server">
        </asp:textbox>
    </div>

    <asp:Button Text="Add Character" runat="server" OnClick="AddCharacter"/>

    <div class="querybox" id="debug" runat="server">

    </div>

</asp:Content>
