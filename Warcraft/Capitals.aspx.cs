﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Warcraft
{
    public partial class Capitals : System.Web.UI.Page
    {
        private string basequery { get; set; } = "SELECT capital_id, capital_name, capital_continent, capital_faction, capital_description FROM capitals";
        protected void Page_Load(object sender, EventArgs e)
        {
            capitals_query.InnerHtml = basequery;
            capitals_select.SelectCommand = basequery;
            //characters_list.DataSource = characters_select;
            capitals_list.DataSource = Capitals_Page_Bind(capitals_select);
            capitals_list.DataBind();
        }
        protected DataView Capitals_Page_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                //row["capital_name"] =
                //    "<a href=\"Class.aspx?classid="
                //    + row["capital_id"]
                //    + "\">"
                //    + row["capital_name"]
                //    + "</a>";
            }
            mytbl.Columns.Remove("capital_id");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}