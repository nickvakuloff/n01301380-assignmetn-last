﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Capitals.aspx.cs" Inherits="Warcraft.Capitals" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="capitals_select"
       ConnectionString="<%$ ConnectionStrings:warcraft_sql_con %>">
    </asp:SqlDataSource>
    <div id="capitals_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="capitals_list" runat="server" >
    </asp:DataGrid>
</asp:Content>
